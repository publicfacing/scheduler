window.dataSync = {
	syncURL: "http://apps.wintergroup.net/schedulr/api/conferences",
	
    initialize: function(callback) {
        var self = this;
        this.db = window.openDatabase("schedulr_1_0_conf", "1.0", "myconferences", 200000);


		console.log("database opened");
        // Testing if the table exists is not needed and is here for logging purpose only. We can invoke createTable
        // no matter what. The 'IF NOT EXISTS' clause will make sure the CREATE statement is issued only if the table
        // does not already exist.
        this.db.transaction(
            function(tx) {
                tx.executeSql("SELECT name FROM sqlite_master WHERE type='table' AND name='schedulr_1_0_conf'", this.txErrorHandler,
                    function(tx, results) {
						self.createTable(callback)
                    });
				tx.executeSql("SELECT name FROM sqlite_master WHERE type='table' AND name='schedulr_1_0_fav'", this.txErrorHandler,
                    function(tx, results) {
						self.createFav(callback);
                     });
            }
        )
    },
        
    createTable: function(callback) {
        this.db.transaction(
            function(tx) {
                var sql =
                  "CREATE TABLE IF NOT EXISTS schedulr_1_0_conf ( " +
					"id INTEGER PRIMARY KEY AUTOINCREMENT, " +
					"cname TEXT, " +
					"smonth TEXT, " +
					"sday INTEGER, " +
					"syear INTEGER, " +
					"sdayname TEXT, " +
					"emonth TEXT, " +
					"eday INTEGER, " +
					"eyear INTEGER, " +
					"edayname TEXT, " +
					"description TEXT, " +
					"category TEXT, " +
					"locname TEXT, " +
					"street TEXT, " +
					"city TEXT, " +
					"state TEXT, " +
					"zip INTEGER, " +
					"weblink TEXT, " +
					"datapoint TEXT, " +
					"attendance TEXT, " +
					"exhibdl TEXT, " +
					"regdl TEXT, " +
					"hotelbookdl TEXT, " +
					"speakpropdl TEXT, " +
					"contactname TEXT, " +
					"contactemail TEXT, " +
					"contactphone TEXT, " +
					"lastmodified VARCHAR(50))";
				tx.executeSql(sql);
				console.log('Database schedulr_1_0_conf found or created');          				
            },
            this.txErrorHandler,
            function() {
                callback();
            }
        );
    },
	
	createFav: function(callback) {
        this.db.transaction(
            function(tx) {
                var sql =
                  "CREATE TABLE IF NOT EXISTS schedulr_1_0_fav ( " +
					"id INTEGER PRIMARY KEY AUTOINCREMENT, " +
					"cid INTEGER)";
				tx.executeSql(sql);
				console.log('Database schedulr_1_0_fav found or created');
            },
            this.txErrorHandler,
            function() {
                callback();
            }
        );
    },

    dropTable: function(callback) {
        this.db.transaction(
            function(tx) {
                tx.executeSql('DROP TABLE IF EXISTS schedulr_1_0_conf');
				console.log('Database schedulr_1_0_fav dropped');
            },
            this.txErrorHandler,
            function() {
                callback();
            }
        );
    },

    findAll: function(callback) {
        this.db.transaction(
            function(tx) {
                var sql = "SELECT * FROM schedulr_1_0_conf";
                tx.executeSql(sql, this.txErrorHandler,
                    function(tx, results) {
                        var len = results.rows.length,
                            conferences = [],
                            i = 0;
                        for (; i < len; i = i + 1) {
                            conferences[i] = results.rows.item(i);
                        }
                        callback(conferences);
                    }
                );
            }
        );
    },
	
	findFav: function(callback) {
        this.db.transaction(
            function(tx) {
                var sql = "SELECT cid FROM schedulr_1_0_fav";
                tx.executeSql(sql, this.txErrorHandler,
                    function(tx, results) {
                        var len = results.rows.length,
                           favorites = [],
                            i = 0;
                        for (; i < len; i = i + 1) {
                            favorites[i] = results.rows.item(i);
                        }
                       callback(favorites);
                    }
                );
            }
        );
    },

    getLastSync: function(callback) {
        this.db.transaction(
            function(tx) {
                var sql = "SELECT MAX(lastmodified) as lastSync FROM schedulr_1_0_conf";
                tx.executeSql(sql, this.txErrorHandler,
                    function(tx, results) {
                        var lastSync = results.rows.item(0).lastSync;
                        callback(lastSync);
                    }
                );
            }
        );
    },

    sync: function(callback) {

        var self = this;
        console.log('Starting synchronization...');
        this.getLastSync(function(lastSync){
            self.getChanges(self.syncURL, lastSync,
                function (changes) {
                    if (changes.length > 0) {
                        self.applyChanges(changes, callback);
                    } else {
                        callback();
                    }
                }
            );
        });

    },

    getChanges: function(syncURL, modifiedSince, callback) {
        $.ajax({
            url: syncURL,
            data: {modifiedSince: modifiedSince},
            dataType:"json",
            success:function (data) {
                callback(data);
				console.log('AJAX success');
            },
            error: function(model, response) {
                console.log('AJAX alert: '+response.responseText);
            }
        });

    },
    applyChanges: function(conferences, callback) {
        this.db.transaction(
            function(tx) {
                var l = conferences.length;
                var sql =
                    "INSERT OR REPLACE INTO schedulr_1_0_conf (id, cname, smonth, sday, syear, sdayname, emonth, eday, eyear, edayname, description, category, locname, street, city, state, zip, weblink, datapoint, attendance, exhibdl, regdl, hotelbookdl, speakpropdl, contactname, contactemail, contactphone, lastmodified) " +
                    "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
                var e;
                for (var i = 0; i < l; i++) {
                    e = conferences[i];
                    var params = [e.id, e.cname, e.smonth, e.sday, e.syear, e.sdayname, e.emonth, e.eday, e.eyear, e.edayname, e.description, e.category, e.locname, e.street, e.city, e.state, e.zip, e.weblink, e.datapoint, e.attendance, e.exhibdl, e.regdl, e.hotelbookdl, e.speakpropdl, e.contactname, e.contactemail, e.contactphone, e.lastmodified];
                    tx.executeSql(sql, params);
                }
            },
            this.txErrorHandler,
            function(tx) {
                callback();
            }
        );
    },
	
	applyFavorite: function(itemId, conferences) {
        this.db.transaction(
            function(tx) {
				//var sql = "";
				var sql = "INSERT OR REPLACE INTO schedulr_1_0_fav (cid) VALUES ("+itemId+")";
				tx.executeSql(sql);
			}
        );
    },
	removeFavorite: function(itemId, conferences) {
        this.db.transaction(
            function(tx) {
				//var sql = "";
				var sql = "DELETE FROM schedulr_1_0_fav WHERE cid = '"+itemId+"'";
				tx.executeSql(sql);
			}
        );
    },
	
	renderFavorites: function(conferences, favorites) {
        var conflist = [];
        var favlist = [];
        var fnllist = [];
        var fav = '';
        var generateFav = '';
        var date = [];
        var uniqueDate = [];
		var year = [];
		var uniqueYear = [];
		var favoritesmonth = '';
        
        dataSync.findFav(function(favorites){
            $.each(favorites, function(i, favid){
                favlist.push(favid.cid);
            });//each
        
            dataSync.findAll(function(conferences) {
                $.each(conferences, function(i, item){
                        conflist.push(item);
			  });//each
            
                $.each(conflist, function(i, item){
                    var confid = item.id;
                    if ($.inArray(confid, favlist) > -1){ 
                        fnllist.push(item);
                    }
                });
                
                $.each(fnllist, function(i,item){
                    var favsmonth = item.smonth.replace(/\s+/, "") + ' ' + item.syear;
						  
                    date.push(favsmonth);
                    $.each(date, function(i, el){
                        if($.inArray(el, uniqueDate) === -1) uniqueDate.push(el);
                    });
				});
				
				$.each(fnllist, function(i,item){
					var syear = item.syear;  
					year.push(syear);
					$.each(year, function(i, el){
						if($.inArray(el, uniqueYear) === -1) uniqueYear.push(el);
					});
				});
				uniqueYear.sort();
                
				var months = new Array(12);
					months['Jan'] = 1;
					months['Feb'] = 2;
					months['Mar'] = 3;
					months['Apr'] = 4;
					months['May'] = 5;
					months['Jun'] = 6;
					months['Jul'] = 7;
					months['Aug'] = 8;
					months['Sep'] = 9;
					months['Oct'] = 10;
					months['Nov'] = 11;
					months['Dec'] = 12;

                function sortDate(a,b)
                    {
                        var m1 = a.substring(0,3);
                        var y1 = a.substring(4);
                        var m2 = b.substring(0,3);
                        var y2 = b.substring(4);    
                        
                        if(Number(y1)>Number(y2)) {
                            return 1;
                        } else if(Number(y1)<Number(y2)) {
                            return -1;
                        } else {
                            if(months[m1]>months[m2]) {
                                return 1;
                            } else if(months[m1]<months[m2]) {
                                return -1;
                            }
                        }
                        return 0;
                    }
				uniqueDate.sort(sortDate);
                
				$.each(uniqueYear, function(i, item){
					var currentYearMarker = item;
					generateFav += '<li class="fav-divider year">' + currentYearMarker + '</li>';
					$.each(uniqueDate, function(i, item){
						var currentMarker = item;
						var clippedMarker = currentMarker.substring(0, currentMarker.length - 5);
						if (currentMarker.indexOf(currentYearMarker) !== -1){
							generateFav += '<li class="fav-divider">' + clippedMarker + '</li>';
						}//if
						
						function sortOn (arr, prop) {
								fnllist.sort (
									function (a, b) {
										if (a[prop] < b[prop]){
											return -1;
										} else if (a[prop] > b[prop]){
											return 1;
										} else {
											return 0;   
										}
									}
								);
							}
							sortOn("item", "sday");
						
						$.each(fnllist, function(i, item){
							favoritesmonth = item.smonth + ' ' + item.syear;
                   
						if (favoritesmonth == currentMarker && currentMarker.indexOf(currentYearMarker) !== -1) {                     
							generateFav += '<li class="search-item"><a href="#confpage-' + item.id + '" ><span class="fav-day">' 
								+ item.sday + '</span><span class="fav-name">' + item.cname + '</span></a></li>';
						}
					});//each
						
				});//each
			});//each

               if (generateFav === ''){
                    var newContent = '<h1 class="fav-banner">Favorites</h1><ul id="favul" data-role="listview" data-inset="true">' + '<li class="no-item">There are no conferences added to Favorites</li>' + '</ul>';
                    $('#myconferencelistcontent').html(newContent).trigger("create");   
               }//if
               else{
                   newContent = '<h1 class="fav-banner">Favorites</h1><ul id="favul" data-role="listview" data-inset="true" >' + generateFav + '</ul>';
                   $('#myconferencelistcontent').html(newContent).trigger("create");
               }//else
            });//findAll
        });//findFav
    },//renderFavorites
	
	renderSearchbyName: function(conferences) {
		var generate='';
		var generateHeader = '';
		var generatePage='';
		var generateFooter='';
		var generateFooterLinks='';
		var generateFooterLinksSum='';
		var generateFullPage ='';
		var alphabet = [];
		var uniqueAlphabet = [];
        dataSync.findAll(function(conferences) {
            $.each(conferences, function(i,item){ 
                alphabet.push((item.cname).slice(0,1));
                $.each(alphabet, function(i, el){
                    if($.inArray(el, uniqueAlphabet) === -1) uniqueAlphabet.push(el);
                    });
                });
            uniqueAlphabet.sort();
            for (var i=0; i < uniqueAlphabet.length; i++){
                var currentMarker = uniqueAlphabet[i];
                generateFooterLinks += '<li><a href="#dividerpage-' + currentMarker 
									+ '" data-transition="fade">' + uniqueAlphabet[i] + '</a></li>';
            }
            generateFooterLinksSum = generateFooterLinks;
				for (var i=0; i < uniqueAlphabet.length; i++){
					var currentMarker = uniqueAlphabet[i];
                    generate += '<li>' + '<a href="#dividerpage-' + currentMarker + '">' + uniqueAlphabet[i] + '</a></li>';
                    generateFooterLinks += '<li><a href="#dividerpage-' + currentMarker 
                        + '"  data-transition="fade">' + uniqueAlphabet[i] + '</a></li>';
                    generateHeader = '<div data-role="page" id="dividerpage-' + currentMarker + '" class="conf-pages sbn-pages">'
						+ '<div data-role="header" data-id="conf_header" >'
                        + '<a href="#confsbn-' + currentMarker + '" class="menu-icon" data-role="none"></a>'
                        + '<h1>By Name</h1>'
                        + '</div>'
                        + '<div data-role="content">'
                        + '<h4 class="search-title by-name">Search by Name: ' + currentMarker + '</h4>'
                        + '<ul data-role="listview" data-inset="true" class="search-content-links" >';
                    $.each(conferences, function(i,item){
                        function sortOn (arr, prop) {
                            conferences.sort (
                                function (a, b) {
                                    if (a[prop] < b[prop]){
                                        return -1;
                                    } else if (a[prop] > b[prop]){
                                        return 1;
                                    } else {
                                        return 0;   
                                    }
                                }
                            );
                        }
                        sortOn("item", "cname");									 
                        if ((item.cname).slice(0,1) === currentMarker) {
                            generatePage += '<li class="search-item"><a href="#confpage-' + item.id + '" >' 
                                + item.cname + '</a></li>';
                        }
                    });
									
                    generateFooter = '</div><div data-role="footer" data-theme="a" id="conf_footer" data-position="fixed">'
                        + '<div data-role="navbar">'
                        + '<ul>'
                        + '<li class="border-right"><a href="#myconferencelist" id="chat" ><img src="img/footer-btn-favorites-up.jpg" /></a></li>'
                        + '<li class="border-right"><a href="#search" id="email" ><img src="img/footer-btn-search-up.jpg" /></a></li>'
                        + '<li><a href="#main" id="skull" ><img src="img/footer-btn-home-up.jpg" /></a></li>'
                        + '</ul>'
                        + '</div>'		
                        + '</div>'
                        + '<div data-role="panel" data-position-fixed="true" data-display="overlay" data-theme="a" id="confsbn-' + currentMarker + '" class="ui-responsive-panel">'
                        + '<ul data-role="listview" data-theme="a" class="nav-search">'
                        + '<li data-icon="delete"><a href="#" data-rel="close">Close menu</a></li>'
                        + generateFooterLinksSum
                        + '<li data-icon="back"><a href="#" data-rel="back">Back</a></li>'
                        + '</ul>'
                        + '</div>'
									
                        + '</div>';
                    generatePage += '<li data-icon="back" class="search-item"><a class="btn-back" data-rel="back" data-transition="fade">back</a></li>';
                    generateFullPage = generateHeader + generatePage + generateFooter;
                    var pageId = '#dividerpage-' + currentMarker;
                    ($.mobile.pageContainer).append(generateFullPage);
                    generatePage = '';
                }
            var newContent = '<ul data-role="none" data-inset="true" >' + generate + '</ul>';
            $('#searchbynamecontent').html(newContent).trigger("create");
        });
	},
	
	renderSearchbyDate: function(conferences) {
		var generate='';
		var generateHeader = '';
		var generatePage='';
		var generateFooter='';
		var generateFooterLinks='';
		var generateFullPage ='';
		var date = [];
		var uniqueDate = [];
		var sbdyear = [];
		var sbduniqueYear = [];
			dataSync.findAll(function(conferences) {
				$.each(conferences, function(i,item){
					if (item.smonth !== ""){ 
                       date.push(item.smonth);
					}
					
					for (var i=0; i < date.length; i++){
						var currentItem = date[i];
						if(currentItem.indexOf(' ') >= 0){
                            date[i] = currentItem.replace(/\s/g, "");
						}
					}
					
					$.each(date, function(i, el){
						if($.inArray(el, uniqueDate) === -1) uniqueDate.push(el);
						});
					});
					
					var months = new Array(12);
						months['Jan'] = 1;
						months['Feb'] = 2;
						months['Mar'] = 3;
						months['Apr'] = 4;
						months['May'] = 5;
						months['Jun'] = 6;
						months['Jul'] = 7;
						months['Aug'] = 8;
						months['Sep'] = 9;
						months['Oct'] = 10;
						months['Nov'] = 11;
						months['Dec'] = 12;

                    function sortDate(a,b)
						{
							var m1 = a.substring(0,3);
							var y1 = a.substring(4);
							var m2 = b.substring(0,3);
							var y2 = b.substring(4);	
						
							if(Number(y1)>Number(y2)) {
								return 1;
							} else if(Number(y1)<Number(y2)) {
								return -1;
							} else {
								if(months[m1]>months[m2]) {
									return 1;
								} else if(months[m1]<months[m2]) {
									return -1;
								}
							}
							return 0;
						}
                uniqueDate.sort(sortDate);
				for (var i=0; i < uniqueDate.length; i++){
					var currentMarker = uniqueDate[i];
					generateFooterLinks += '<li><a href="#dividerpage-' + currentMarker 
									+ '"  data-transition="fade">' + uniqueDate[i] + '</a></li>';
				}
				
				$.each(conferences, function(i,item){
					var syear = item.syear;
					sbdyear.push(syear);
					$.each(sbdyear, function(i, el){
						if(el === 0){
						sbdyear.pop(el);	
						}
					});
					$.each(sbdyear, function(i, el){
						if($.inArray(el, sbduniqueYear) === -1) sbduniqueYear.push(el);
					});
				});
				sbduniqueYear.sort();
				
				for (var i=0; i < uniqueDate.length; i++){
					var currentMarker = uniqueDate[i];
                    generate += '<li>' + '<a href="#dividerpage-' + currentMarker + '">' + uniqueDate[i] + '</a></li>';
                    generateHeader = '<div data-role="page" id="dividerpage-' + currentMarker + '" class="conf-pages sbd-pages">'
						+ '<div data-role="header" data-id="conf_header" >'
                        + '<a href="#confsbd-' + currentMarker + '" class="menu-icon" data-role="none"></a>'
                        + '<h1>By Date</h1>'
                        + '</div>'
                        + '<div data-role="content">'
                        + '<h4 class="search-title by-date">Search by Date: ' + currentMarker + '</h4>'
                        + '<ul data-role="listview" data-inset="true" class="search-content-links" >';
											
                    var year = [];
                    var uniqueYear = [];
					  var eventsFired = 0;	
					  
					  $.each(sbduniqueYear, function(i, item){
					var currentYearMarker = item;
					generatePage += '<li class="fav-divider year">' + currentYearMarker + '</li>';
					  					
                    $.each(conferences, function(i,item){
                        function sortOn (arr, prop) {
                            conferences.sort (
                                function (a, b) {
                                    if (a[prop] < b[prop]){
                                        return -1;
                                    } else if (a[prop] > b[prop]){
                                        return 1;
                                    } else {
                                        return 0;   
                                    }
                                }
                            );
                        }
                        sortOn("item", "sday");
							
                        var currentDate = item.smonth.replace(/\s/g, "");	
						  var currentsbdYear = item.syear;								 
                        if (currentDate === currentMarker && currentsbdYear === currentYearMarker) {
                            generatePage += '<li class="search-item search-date"><a href="#confpage-' + item.id + '" >' 
                                + '<span class="fav-day">' + item.sday + '</span><span class="fav-name">' + item.cname + '</span></a></li>';
                            }
                    });
					  });
									
                    generateFooter = '</div><div data-role="footer" data-theme="a" id="conf_footer" data-position="fixed">'
                        + '<div data-role="navbar">'
                        + '<ul>'
                        + '<li class="border-right"><a href="#myconferencelist" id="chat" ><img src="img/footer-btn-favorites-up.jpg" /></a></li>'
                        + '<li class="border-right"><a href="#search" id="email" ><img src="img/footer-btn-search-up.jpg" /></a></li>'
                        + '<li><a href="#main" id="skull" ><img src="img/footer-btn-home-up.jpg" /></a></li>'
                        + '</ul>'
                        + '</div>'		
                        + '</div>'
                        + '<div data-role="panel" data-position-fixed="true" data-display="overlay" data-theme="a" id="confsbd-' + currentMarker + '" class="ui-responsive-panel">'
                        + '<ul data-role="listview" data-theme="a" class="nav-search">'
                        + '<li data-icon="delete"><a href="#" data-rel="close">Close menu</a></li>'
                        + generateFooterLinks
                        + '<li data-icon="back"><a href="#" data-rel="back">Back</a></li>'
                        + '</ul>'
                        + '</div>'
                        + '</div>';
                    generatePage += '<li data-icon="back" class="search-item"><a class="btn-back" data-rel="back" data-transition="fade">back</a></li>';
                    generateFullPage = generateHeader + generatePage + generateFooter;
                    var pageId = '#dividerpage-' + currentMarker;
                    ($.mobile.pageContainer).append(generateFullPage);
                    generatePage = '';
                }
				var newContent = '<ul data-role="none" data-inset="true" >' + generate + '</ul>';
                $('#searchbydatecontent').html(newContent).trigger("create");
				$('li.fav-divider').each(function(){
					if(! $(this).next('li').hasClass('search-date') ){
						$(this).remove();
					}
				});
        });
	},
	
	renderSearchbyCategory: function(conferences) {
		var generate='';
		var generateHeader = '';
		var generatePage='';
		var generateFooter='';
		var generateFooterLinks='';
		var generateFooterLinksSum='';
		var generateFullPage ='';
		var category = [];
		var uniqueCategory = [];
		var uniqueCategoryLink = [];
		var splitArray = [];
		dataSync.findAll(function(conferences) {
			$.each(conferences, function(i,item){
				
				if (item.category !== ""){  
					category.push(item.category);
				}
				
				for (var i=0; i < category.length; i++){
					var currentItem = category[i];
					category[i] = currentItem.replace(/^\s+|\s+$/g,'');
				}
				
				for (var i=0; i < category.length; i++){
					var currentItem = category[i];
					if (currentItem.indexOf('|') !== -1){
						currentItem = currentItem.split('|');
						$.each(currentItem, function(i,item){
							category.push(currentItem[i]);
						});
					}
				}
				for (var i=0; i < category.length; i++){
					var currentItem = category[i];
					if (currentItem.indexOf('|') >= 0){
						var removeItem = currentItem;
						category = $.grep(category, function(value) {
							return value != removeItem;
						});
					}
				}
				
				$.each(category, function(i, el){
					if($.inArray(el, uniqueCategory) === -1) uniqueCategory.push(el);
				});
			});
			uniqueCategory.sort();
			for (var i=0; i < uniqueCategory.length; i++){
				var currentMarker = uniqueCategory[i];
				var currentLink = currentMarker.replace(/\s/g, "-").replace(/\:/g, "").replace(/\,/g, "").replace(/\&/g, "").replace(/\"/g, "");
				generateFooterLinks += '<li><a href="#dividerpage-' + currentLink 
					+ '"  data-transition="fade">' + uniqueCategory[i] + '</a></li>';
			}
			
			generateFooterLinksSum = generateFooterLinks;
			for (var i=0; i < uniqueCategory.length; i++){
				var currentMarker = uniqueCategory[i];
				var currentLink = currentMarker.replace(/\s/g, "-").replace(/\:/g, "").replace(/\,/g, "").replace(/\&/g, "").replace(/\"/g, "");
				generate += '<li>' + '<a href="#dividerpage-' + currentLink + '">' + uniqueCategory[i] + '</a></li>';
				generateHeader = '<div data-role="page" id="dividerpage-' + currentLink + '" class="conf-pages sbc-pages">'
					+ '<div data-role="header" data-id="conf_header" >'
					+ '<a href="#confsbc-' + currentLink + '" class="menu-icon" data-role="none"></a>'
					+ '<h1>By Category</h1>'
					+ '</div>'
			
					+ '<div data-role="content">'
					+ '<h4 class="search-title by-cat">Search by Category: ' + currentMarker + '</h4>'
					+ '<ul data-role="listview" data-inset="true" class="search-content-links" >';
				$.each(conferences, function(i,item){
						function sortOn (arr, prop) {
								conferences.sort (
										function (a, b) {
										if (a[prop] < b[prop]){
											return -1;
										} else if (a[prop] > b[prop]){
											return 1;
										} else {
											return 0;   
										}
									}
								);
							}
							sortOn("item", "cname");
							var currentCategory = item.category;									 
							if (currentCategory.indexOf(currentMarker) !== -1) {
								generatePage += '<li class="search-item"><a href="#confpage-' + item.id + '" >' 
									+ item.cname + '</a></li>';
							}
					});
								
					generateFooter = '</div><div data-role="footer" data-theme="a" id="conf_footer" data-position="fixed">'
					+ '<div data-role="navbar">'
					+ '<ul>'
					+ '<li class="border-right"><a href="#myconferencelist" id="chat" ><img src="img/footer-btn-favorites-up.jpg" /></a></li>'
					+ '<li class="border-right"><a href="#search" id="email" ><img src="img/footer-btn-search-up.jpg" /></a></li>'
					+ '<li><a href="#main" id="skull" ><img src="img/footer-btn-home-up.jpg" /></a></li>'
					+ '</ul>'
					+ '</div>'		
					+ '</div>'
					
					+ '<div data-role="panel" data-position-fixed="true" data-display="overlay" data-theme="a" id="confsbc-' + currentLink + '" class="ui-responsive-panel">'
					+ '<ul data-role="listview" data-theme="a" class="nav-search">'
					+ '<li data-icon="delete"><a href="#" data-rel="close">Close menu</a></li>'
					+ generateFooterLinksSum
					+ '<li data-icon="back"><a href="#" data-rel="back">Back</a></li>'
					+ '</ul>'
					+ '</div>'
					
					+ '</div>';
					generatePage += '<li data-icon="back" class="search-item"><a class="btn-back" data-rel="back" data-transition="fade">back</a></li>';
					generateFullPage = generateHeader + generatePage + generateFooter;
					var pageId = '#dividerpage-' + currentLink;
					($.mobile.pageContainer).append(generateFullPage);
					/*$.mobile.changePage(pageId);*/
					generatePage = '';
				}
			var newContent = '<ul data-role="none" data-inset="true" >' + generate + '</ul>';
			$('#searchbycategorycontent').html(newContent).trigger("create");
		});
	},
	
    txErrorHandler: function(tx) {
        alert(tx.message);
    }
};

dataSync.initialize(function() {
    console.log('database initialized');
});

function renderList() {
	var generatePage = '';
	var favlist = [];
	var groupFooterLinks = '';
	var alphabet = [];
	var uniqueAlphabet = [];
	var date = [];
	var uniqueDate = [];
	var category = [];
	var uniqueCategory = [];
	dataSync.findFav(function(favorites){
			$.each(favorites, function(i, favid){
				favlist.push(favid.cid);
			});//each
		});
    dataSync.findAll(function(conferences) {
		//generate group footer links	
	$.each(conferences, function(i,item){ 
		alphabet.push((item.cname).slice(0,1));
		$.each(alphabet, function(i, el){
			if($.inArray(el, uniqueAlphabet) === -1) uniqueAlphabet.push(el);
			});
		});
	uniqueAlphabet.sort();
	for (var i=0; i < uniqueAlphabet.length; i++){
		var currentMarker = uniqueAlphabet[i];
		groupFooterLinks += '<li class="name-footer-links"><a href="#dividerpage-' + currentMarker 
			+ '" data-transition="fade">' + uniqueAlphabet[i] + '</a></li>';
	}
	$.each(conferences, function(i,item){
		if (item.smonth !== ""){ 
		   date.push(item.smonth);
		}
		
		for (var i=0; i < date.length; i++){
			var currentItem = date[i];
			if(currentItem.indexOf(' ') >= 0){
				date[i] = currentItem.replace(/\s/g, "");
			}
		}
		
		$.each(date, function(i, el){
			if($.inArray(el, uniqueDate) === -1) uniqueDate.push(el);
			});
		});
		var months = new Array(12);
				months['Jan'] = 1;
				months['Feb'] = 2;
				months['Mar'] = 3;
				months['Apr'] = 4;
				months['May'] = 5;
				months['Jun'] = 6;
				months['Jul'] = 7;
				months['Aug'] = 8;
				months['Sep'] = 9;
				months['Oct'] = 10;
				months['Nov'] = 11;
				months['Dec'] = 12;

			function sortDate(a,b)
				{
					var m1 = a.substring(0,3);
					var y1 = a.substring(4);
					var m2 = b.substring(0,3);
					var y2 = b.substring(4);	
				
					if(Number(y1)>Number(y2)) {
						return 1;
					} else if(Number(y1)<Number(y2)) {
						return -1;
					} else {
						if(months[m1]>months[m2]) {
							return 1;
						} else if(months[m1]<months[m2]) {
							return -1;
						}
					}
					return 0;
				}
		uniqueDate.sort(sortDate);
		for (var i=0; i < uniqueDate.length; i++){
			var currentMarker = uniqueDate[i];
			groupFooterLinks += '<li class="date-footer-links"><a href="#dividerpage-' + currentMarker 
			+ '"  data-transition="fade">' + uniqueDate[i] + '</a></li>';
		}
		$.each(conferences, function(i,item){
				
				if (item.category !== ""){  
					category.push(item.category);
				}
				
				for (var i=0; i < category.length; i++){
					var currentItem = category[i];
					category[i] = currentItem.replace(/^\s+|\s+$/g,'');
				}
				
				for (var i=0; i < category.length; i++){
					var currentItem = category[i];
					if (currentItem.indexOf('|') !== -1){
						currentItem = currentItem.split('|');
						$.each(currentItem, function(i,item){
							category.push(currentItem[i]);
						});
					}
				}
				for (var i=0; i < category.length; i++){
					var currentItem = category[i];
					if (currentItem.indexOf('|') >= 0){
						var removeItem = currentItem;
						category = $.grep(category, function(value) {
							return value != removeItem;
						});
					}
				}
				$.each(category, function(i, el){
					if($.inArray(el, uniqueCategory) === -1) uniqueCategory.push(el);
				});
			});
			uniqueCategory.sort();
			for (var i=0; i < uniqueCategory.length; i++){
				var currentMarker = uniqueCategory[i];
				var currentLink = currentMarker.replace(/\s/g, "-").replace(/\:/g, "").replace(/\,/g, "").replace(/\&/g, "").replace(/\"/g, "");
				
				groupFooterLinks += '<li class="category-footer-links"><a href="#dividerpage-' + currentLink 
					+ '"  data-transition="fade">' + uniqueCategory[i] + '</a></li>';
			}	
		
     	$.each(conferences, function(i,item){ 
			generatePage = '<div data-role="page" id="confpage-' + item.id + '" class="conf-pages conf-main-pages">'
				+ '<div data-role="header" data-id="conf_header" >'
				+ '<a href="#conf-' + item.id + '" class="menu-icon-low" data-role="none"></a>'
				+ '<h1 class="conf-header">' + item.cname + '</h1>'
				+ '</div>'
				+ '<div data-role="content" id="conf-page-container">';
										
			generatePage += '<div class="flexbox">';
			if (item.sdayname !== ''){
				generatePage += '<div class="col"><p><strong>Dates: </strong><span class="small">' + item.sdayname + ', ' + item.smonth + ' ' 
				+ item.sday + ', ' + item.syear + ' — ' 
				+ item.edayname + ', ' + item.emonth + ' ' + item.eday + ', ' + item.eyear + '</span></p></div>';
			}else{
				generatePage += '<div class="col"><p><strong>Dates: </strong><span class="small">' + 'Not currently available.' + '</span></p></div>';
			};
									
			if (item.locname !== ''){
				generatePage += '<div class="col" ><p><strong>Where: </strong><span class="small">' + '<b>' + item.locname + '</b>' + '</br >' + item.street + ', ' 
					+ item.city + ', ' + item.state + ' ' + item.zip + '</span></div>';
			}else{
				generatePage += '<div class="col" ><p><strong>Where: </strong><span class="small">' + 'Not currently available.' + '</span></p></div>';
			};
									
			generatePage += '</div>' //flexbox
			generatePage += '<div class="tab-wrapper">'
				+ '<div data-role="navbar" id="navbar">'
				+ '<ul>'
				+ '<li><a href="#" class="ui-btn-active first-tab" data-tab-class="tab1">From the Site</a></li>'
				+ '<li class="tabtwo"><a href="#" data-tab-class="tab2" class="label-2">Deadlines</a></li>'
				+ '<li class="tabthree"><a href="#" data-tab-class="tab3" class="label-3">Contact</a></li>'
				+ '</ul>'
				+ '</div>'
				+ '<div class="conf-tab">'
				+ '<div class="tab1">';
			if (item.description !== ''){
				generatePage += '<p>' + item.description + '</p>'
			}else{
				generatePage += '<p>Not currently available.</p>'
			};
			if (item.attendance !== ''){
				generatePage += '<p><b>Last Year\'s Attendance:</b> ' + item.attendance + '</p>';
			}else{};
										
			generatePage += '</div>'//Tab 1
				+ '<div class="tab2 ui-screen-hidden">';			
			if (item.exhibdl !== '' || item.regdl !== '' || item.hotelbookdl !== '' || item.speakpropdl !== ''){
				if (item.exhibdl !== ''){
					generatePage += '<p>Exhibit Deadline: ' + item.exhibdl + '</p>';
				}else{};
				if (item.regdl !== ''){
					generatePage += '<p>Registration Deadline: ' + item.regdl + '</p>';
				}else{};
				if (item.hotelbookdl !== ''){
					generatePage += '<p>Hotel Booking Deadline: ' + item.hotelbookdl + '</p>';
				}else{};
				if (item.speakpropdl !== ''){
					generatePage += '<p>Speaker Proposed Deadline: ' + item.speakpropdl + '</p>';
				}else{};
			}else{
				generatePage += '<p>Not currently available.</p>'
			};
			generatePage += '</div>'//Tab 2
				+ '<div class="tab3 ui-screen-hidden">';
			if (item.contactname !== '' || item.contactemail !== '' || item.contactphone !== ''){
				if (item.contactname !== ''){
					generatePage += '<p>Contact Name: ' + item.contactname + '</p>';
				}else{};
				if (item.contactemail !== ''){
					generatePage += '<p>Contact Email: ' + item.contactemail + '</p>';
				}else{};
				if (item.contactphone !== ''){
					generatePage += '<p>Contact Phone: ' + item.contactphone + '</p>';
				}else{};
			}else{generatePage += '<p>Not currently available.</p>'};
			generatePage += '</div>'//Tab 3
			generatePage += '</div>'//conf-tab
				+ '</div>';//tab-wrapper
			generatePage += '<div class="flexbox">';
			if (item.weblink !== ''){
				generatePage += '<div class="col colweblink"><a class="ext" href="#" target="_blank" data-link="' + item.weblink + '"><p>' + 'Visit Conference Site' + '</p></a></div>';
			}else{
				generatePage += '<div class="col colweblink"><p>' 
					+ 'Conference Site Not Currently Available' + '</p></div>';
			}
			var currentId = item.id;
			if ($.inArray(currentId, favlist) > -1){
				generatePage += '<div class="col favbtn" ><a data-cid="' + item.id 
					+ '" class="favoritebtn favorite"  href="#" >Remove from My Favorites</a></div>';
			}else{
				generatePage += '<div class="col favbtn" ><a data-cid="' + item.id 
					+ '" class="favoritebtn"  href="#" >Add to My Favorites</a></div>';
			}
									
			generatePage += '</div>';
			if (item.datapoint !== '' && item.category !== ''){
				generatePage += '<div class="flexbox flexboxtwo">';
				if (item.category.indexOf('|') >= 0){
					var categoryfix = item.category.replace(/[|]/g,', ');
					generatePage += '<div class="col-cat" ><p>Categories: </br><span class="small">' 
						+ categoryfix + '</span></p>' + '</div>';
				}else{
					generatePage += '<div class="col-cat" ><p>Categories: </br><span class="small">' 
						+ item.category + '</span></p>' + '</div>';
				}
				generatePage += '<div class="col coldatapoint"><a class="ext" href="#" target="_blank" data-link="' + item.datapoint + '" >'
					+ '<p>Find relevant leads from <br /><img src="img/mch-fav-logo.png" /></p></a></div>'
					+ '</div>';//flexbox
			}
			else if (item.datapoint !== '' && item.category === ''){
				generatePage += '<a class="ext" href="#" target="_blank" data-link="' + item.datapoint + '"><div class="web-link-att"><p>Find relevant leads from <br /><img src="img/mch-fav-logo.png" /></p></div></a>';
			}
			else if(item.category !== '' && item.datapoint === ''){
				if (item.category.indexOf('|') >= 0){
					var categoryfix = item.category.replace(/[|]/g,', ');
					generatePage += '<div class="web-link">Categories: ' 
						+ categoryfix + '</div>';
				}
				else{generatePage += '<div class="web-link">Categories: ' 
							+ item.category + '</div>';}
				}else{};
			if (item.street !== '' && item.city !== '' && item.state !== '' && item.zip !== '0'){
				generatePage += '<div data-role="collapsible" data-collapsed="true"'
					+ 'class="map-collapsible">'
					+ '<h4 class="map-button">Map</h4><ul data-role="listview" data-inset="true">';
				generatePage += '<li><div class="map-canvas" data-conferences-map-address="' + item.street + ', ' + item.city + ', '
					+ item.state + ', ' + item.zip + '" ></div></li></ul></div>';
			}else{};// map
									
			generatePage += '<ul data-role="listview" data-inset="true" class="search-content-links" ><li data-icon="back" class="search-item"><a class="btn-back" data-rel="back" data-transition="fade">back</a></li></ul>'
			generatePage += '</div>';//content
						
			generatePage += '<div data-role="footer" data-theme="a" id="conf_footer" data-position="fixed">'
				+ '<div data-role="navbar">'
				+ '<ul>'
				+ '<li class="border-right"><a href="#myconferencelist" class="favorite-footer-icon" ></a></li>'
				+ '<li class="border-right"><a href="#search" class="search-footer-icon" ></a></li>'
				+ '<li><a href="#main" class="home-footer-icon" ></a></li>'
				+ '</ul>'
				+ '</div>'		
				+ '</div>'
				+ '<div data-role="panel" data-position-fixed="true" data-display="overlay" data-theme="a" id="conf-' + item.id + '" class="ui-responsive-panel" >'
				+ '<ul data-role="listview" data-theme="a" class="nav-search">'
				+ '<li data-icon="delete"><a href="#" data-rel="close">Close menu</a></li>'
				+ '<li data-icon="back"><a href="#" data-rel="back">Back</a></li>'
				+ '<li class="favorite-footer-links"><a href="#main" data-icon="home">Home</a></li>'  
				+ '<li class="favorite-footer-links"><a href="#myconferencelist" >Favorites</a></li>'
				+ '<li class="favorite-footer-links"><a href="#search" >Search</a></li>'
				+ '<li class="favorite-footer-links"><a href="#aboutwintergroup" >About Winter Group</a></li>'
				+ '<li class="favorite-footer-links"><a href="#aboutmch" >About MCH</a></li>'
				+ groupFooterLinks
				+ '</ul>'
				+ '</div>'
				+ '</div>';

			var pageId = '#confpage-' + item.id;
			($.mobile.pageContainer).append(generatePage);
		});
    });
	console.log('Conference Pages Generated');
}
function error(){
	console.log('There was an error loading the data.');
}

function log(msg) {
    $('#log').val($('#log').val() + msg + '\n');
}

