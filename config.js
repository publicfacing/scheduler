module.exports = {
	environment: 'localhost', // (localhost|dev|staging|production)
	port: 63342,
	path_to_core: './node_modules/deathstar/api',
	path_to_ui_core: './node_modules/deathstar/ui',
	project_ui: 'ui',//default ui folder in the project
	project_api: 'api',//default api folder in the project
	ui_type: 'node',//static|node (whether we are using the legacy ui or new node.js served seo friendly ui)
	api_version: 'v1.0', //default api version
	ui_version: 'v1.0', //default api version

	//all the above settings are just defaults that can be overridden  below specifically for each project
	projects: {
		'localhost': {
			siteContext: 'web',
			project_folder: '.',
			environment: 'localhost'
		},
		'localhost.app': {
			siteContext: 'app',
			project_folder: '.',
			environment: 'localhost'
		}
	}
};
